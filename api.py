import requests
from flask import Flask
from flask import request
import re
from html.parser import HTMLParser
import json

try:
    from html import escape  # python 3.x
except ImportError:
    from cgi import escape  # python 2.x

#used to extract the title from a given url
class TitleParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.match = False
        self.title = ''

    def handle_starttag(self, tag, attributes):
        if tag=='title':
            self.match=True

    def handle_data(self, data):
        if self.match:
            self.title = data
            self.match = False

            
class MessageInfo():
    def __init__(self):
        self.mentions=[]
        self.emoticons=[]
        self.links=[]

    def addMention(self,mention):
       
        self.mentions.append(mention)

    def addEmoticon(self,emoticon):
       
        self.emoticons.append(emoticon)

    def addLink(self,url,title):
       
        self.links.append({'url':url,'title':title})

    def toJson(self):
        # this list comprehension rebuild the object with empty keys removed
        # by default, json.dumps would return "null" for None objects, or [] for empty arrays
        return json.dumps(dict([(i,self.__dict__[i]) for i in self.__dict__ if self.__dict__[i]]),indent=4)
        

app = Flask(__name__)
        
@app.route("/",methods=['POST'])
def process():
    msg = request.form['message']
    info = MessageInfo()

    # The given definitions of mentions and emoticons make regex a good fit here

    #look for mentions
    mentions = re.findall('(@\w+)+',msg)
    for m in mentions:
        info.addMention(m.replace("@",""))

    #look for emoticons
    mentions = re.findall('\(\w{0,15}\)',msg)
    for m in mentions:
        info.addEmoticon(m.replace("(","").replace(")",""))

    #regex for urls can be a problematic, so we're going to do some basic tests
    # and then let requests figure out if it's actually valid for us

    # break our message up by whitespace, grab any string that starts with "http"
    urls = [s for s in re.split('\s+',msg) if s[:4]=='http']
    
    for url in urls:
        #Throw this in a blind try/except block
        #We don't want this to error out if we have incorrectly determined something to be a url
        #or the remote host is unreachable for some reason or another
        try:
            req = requests.get(url)
            parser = TitleParser()
            parser.feed(req.text)
            info.addLink(url,escape(parser.title))
        except:
            pass
  
    response = app.response_class(
        # the default key is needed here as the json library doesn't like to serialize user objects as-is
        response=info.toJson(),
        status=200,
        mimetype='application/json'
    )
    return response
    
if __name__ == "__main__":
    app.run()


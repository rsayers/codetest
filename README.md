Overview
========

This is a simple api which parses plain text messages and returns user mentions, emoticons, and link information

Setup
=====

This api will run under Python 2.7 or 3+ with pip installed.

To install the requirements, run `pip -r requirements.txt`

Usage
=====

Run with `python api.py` which should launch the api server on port 5000

POST a message to the root url using x-www-form-urlencoded data, your key will be "message"

Example
=======

```
$curl -X POST -d "message=@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016" localhost:5000
{
    "mentions": [
        "bob",
        "john"
    ],
    "emoticons": [
        "success"
    ],
    "links": [
        {
            "url": "https://twitter.com/jdorfman/status/430511497475670016",
            "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
        }
    ]
}
```

Notes
=====

Given more time, the first thing would be to add a test suite to ensure the api always matches the examples provided in the challenge.  Given even more time, I would have implemented this in Go instead.